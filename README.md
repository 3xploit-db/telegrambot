##telegrambot

<h4>Telegram Bot API CSRF Vulnerability and use it as a ddoser<br>
Home Page : https://core.telegram.org/bots/api<br>
Category : Web Application<br>
==============================<br>
# Description :<br>
==============================<br><h5>
In new version of telegram bot api, a new object called "MessageEntity" permits you that send Urls as<br>
message. By this object, We can achieve GET request from telegram server.<br>
Telegram doesn't check that the image is real or not, and also it doesn't have a captcha or securtiy token<br>
so we can run our php files through Telegram Server<br></h5>
==============================<br>
# Proof Of Concepts :<br>
==============================<br>
In this section, I'll show you that how you can grab telegram Server IP<br>
create a folder named "tg" on your host and create a .htaccess file in that with the following contents:<br>
-------------<br>
Then activate WebHook for you bot and set the "request.php" address for web hook,<br>
Then, Send a message to your bot, if you do everything right, the IP will write on "log.txt" file<br>
<br>
************<br>
************ Exploiting it to ddos<br>
************<br>
<br>
##Change request.php with this:<br>
##and change log.php with this:<br>
--------------------------------------------<br>
<h5>
echo file_get_contents("http://exampledomain.com");
</h5>
---------------------------------------------<br>
<br>
This code will send 100 requests to "exampledomain.com" from telegram IP<br>
You can increase or decrease the request numbers depending on your server features<br>
Also you can exploit it through IRC servers if your server is not strong.<br>
